from td_bank import parse_transactions
from unittest.mock import patch


transaction_lines_mock = [
    ['07/27/2019', 'BRICKWORKS CIDERHOUSE', '44.84', '', '695.65'],
    ['07/30/2019', 'PRETTY UGLY', '52.43', '', '343.81']
]

transactions = [{
    'date': '2019-07-27T00:00:00',
    'comment': 'BRICKWORKS CIDERHOUSE',
    'outcome': '44.84',
    'income': '',
    'balance': '695.65'
}, {
    'date': '2019-07-30T00:00:00',
    'comment': 'PRETTY UGLY',
    'outcome': '52.43',
    'income': '',
    'balance': '343.81'
}]


@patch('td_bank.hasura')
def test_parse_transactions(hasura):
    assert parse_transactions(transaction_lines_mock) == transactions

from flask import Flask, request, render_template, redirect, url_for
from flask_basicauth import BasicAuth
from td_bank import parse_transactions, upload_transactions
import csv
import os
import settings
import uuid

app = Flask(__name__)

app.config['BASIC_AUTH_USERNAME'] = settings.BASIC_AUTH_USERNAME
app.config['BASIC_AUTH_PASSWORD'] = settings.BASIC_AUTH_PASSWORD
app.config['BASIC_AUTH_FORCE'] = True

basic_auth = BasicAuth(app)


@app.route('/')
def index():
    result = request.args.get('result', '')
    return render_template('upload_csv.html', result=result)


@app.route('/csv/<account_id>', methods=['POST'])
def td(account_id):
    f = request.files['data']

    if request.files['data'].content_length == 0:
        return redirect(url_for('.index', result='No file selected!'))

    filename = './temp_{}.csv'.format(uuid.uuid4())
    f.save(filename)

    with open(filename) as csv_file:
        transactions = parse_transactions(csv.reader(csv_file, delimiter=','))
        upload_transactions(
            [{**t, account_id: account_id} for t in transactions]
        )

    os.remove(filename)

    return redirect(url_for('.index', result='Uploaded.'))

import datetime
import hasura


def parse_transactions(transactions_lines):
    return [{
        'date': datetime.datetime.strptime(row[0], '%m/%d/%Y').isoformat(),
        'comment': row[1],
        'outcome': row[2],
        'income': row[3],
        'balance': row[4]
    } for row in transactions_lines]


def build_search_index(expense):
    return expense['income'] + expense['outcome']
    + expense['balance'] + expense['date']


def upload_transactions(transactions):
    query = """
    mutation InsertExpenses($transactions: [expenses_insert_input!]!) {
        insert_expenses(objects: $transactions) {
            returning {
                id
            }
        }
    }
    """

    return hasura.run_query(query, {
        'transactions': transactions
    })


def search_duplicates(transactions):
    query = """
    query SearchDuplicates() {
        expenses(
          where: {
              id: {  _in: ["6ed399b8-c966-4186-8517-58b5091d67dd"] }
            }
        ) {
          id
          comment
        }
    }
    """

    return hasura.run_query(query, {
        'transactions': map(lambda t: t, transactions)
    })

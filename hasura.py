import json
import requests
import settings

headers = {
    'X-Hasura-Admin-Secret': settings.HASURA_GRAPHQL_ADMIN_SECRET,
    'Content-Type': 'application/json'
}


def run_query(query, variables):
    return requests.post(
        settings.HASURA_GRAPHQL_HOST,
        data=json.dumps({
            'query': query,
            'variables': variables
        }),
        headers=headers
    )
